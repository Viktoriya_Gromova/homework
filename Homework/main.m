//
//  main.m
//  Homework
//
//  Created by Admin on 10.10.15.
//  Copyright (c) 2015 Viktoriya Gromova. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        float firstValue = 77.35;
        int secondValue = 8;
        float divide = firstValue / secondValue;
        NSLog(@"\nThe division of two value equals %f", divide);
    }
    return 0;
}


